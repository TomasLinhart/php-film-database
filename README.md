# php-film-database

CN group education - PHP is not dead, let’s create Symfony app

## Folders

### docker
Change pwd to `docker` and run
```bash
docker-compose up --build -d
```

When we want to connect into some container e.g. "webserver" run
```bash
docker-compose run webserver bash
```

### public
Contains two php files for info about webserver.
```url
http://localhost:8000
```
```url
http://localhost:8000/phpinfo.php
```

### film-database
```url
http://localhost:8888
```