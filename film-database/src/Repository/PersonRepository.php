<?php

namespace App\Repository;

use App\Entity\Person;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Person|null find($id, $lockMode = null, $lockVersion = null)
 * @method Person|null findOneBy(array $criteria, array $orderBy = null)
 * @method Person[]    findAll()
 * @method Person[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PersonRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Person::class);
    }
    public function findBornThisMonth(): array
    {
        $now = new DateTime();
        return $this->createQueryBuilder('p')
            ->where('MONTH(p.birthDate) = :month')
            ->setParameter('month', $now->modify('- 2 months')->format('m'))
            ->orderBy('p.birthDate', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }

    public function findRandom(int $limit, array $persons): array
    {
        $queryBuilder = $this->createQueryBuilder('p');
        if (count($persons) > 0) {
            $queryBuilder
                ->where($queryBuilder->expr()->notIn('p.id', array_map(function (Person $person) {
                    return $person->getId();
                }, $persons)));
        }
        return $queryBuilder
            ->orderBy('RAND()')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }

    public function findBySearchTerms(array $searchTerms, int $limit) {
        $queryBuilder = $this->createQueryBuilder('p');

        foreach ($searchTerms as $key => $term) {
            $queryBuilder
                ->orWhere('p.fullName LIKE :t_'.$key)
                ->orWhere('p.birthDate LIKE :t_'.$key)
                ->setParameter('t_'.$key, '%'.$term.'%')
            ;
        }

        return $queryBuilder
            ->orderBy('p.fullName', 'DESC')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }
}
