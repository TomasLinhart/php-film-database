<?php

namespace App\Repository;

use App\Entity\Film;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Film|null find($id, $lockMode = null, $lockVersion = null)
 * @method Film|null findOneBy(array $criteria, array $orderBy = null)
 * @method Film[]    findAll()
 * @method Film[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FilmRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Film::class);
    }

    /**
     * @param int $limit
     * @return array
     * @throws \Exception
     */
    public function findLatest(int $limit): array
    {
        $queryBuilder = $this
            ->createQueryBuilder('f')
            ->where('YEAR(f.year) <= :now')
            ->orderBy('f.year', 'DESC')
            ->setParameter('now', (new DateTime())->format('Y'))
            ->setMaxResults($limit);

        return $queryBuilder->getQuery()->execute();
    }

    /**
     * @param int $limit
     * @return array
     * @throws \Exception
     */
    public function findFuture(int $limit): array
    {
        $queryDQL = $this
            ->getEntityManager()
            ->createQuery(
                'SELECT f
                FROM App\Entity\Film f
                WHERE f.year > :now
                ORDER BY f.year DESC'
            )
            ->setParameter('now', new DateTime())
            ->setMaxResults($limit);

        return $queryDQL->execute();
    }

    public function findBySearchTerms(array $searchTerms, int $limit) {
        $queryBuilder = $this->createQueryBuilder('f');

        foreach ($searchTerms as $key => $term) {
            $queryBuilder
                ->orWhere('f.name LIKE :t_'.$key)
                ->orWhere('YEAR(f.year) LIKE :t_'.$key)
                ->setParameter('t_'.$key, '%'.$term.'%')
            ;
        }

        return $queryBuilder
            ->orderBy('f.name', 'DESC')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }
}
