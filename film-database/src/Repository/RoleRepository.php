<?php

namespace App\Repository;

use App\Entity\Role;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Role|null find($id, $lockMode = null, $lockVersion = null)
 * @method Role|null findOneBy(array $criteria, array $orderBy = null)
 * @method Role[]    findAll()
 * @method Role[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RoleRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Role::class);
    }

    public function findRolesWithUsageCount() {
        return array_map(function ($result) {
            return $result[0]->setUsage($result['usage']);
        },
//        $results[] = [
//        0 => Role,
//        'usage' => 0
//]
            $this->createQueryBuilder('r')
                ->leftJoin('App:FilmPersonRole', 'fpr', 'WITH', 'r.id = fpr.role')
                ->select('r, COUNT(fpr.id) as usage')
                ->groupBy('r.id')
                ->orderBy('r.name', 'ASC')
                ->getQuery()
                ->getResult()
        );
    }
}
