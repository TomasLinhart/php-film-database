<?php

namespace App\Repository;

use App\Entity\Film;
use App\Entity\FilmPersonRole;
use App\Entity\Person;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method FilmPersonRole|null find($id, $lockMode = null, $lockVersion = null)
 * @method FilmPersonRole|null findOneBy(array $criteria, array $orderBy = null)
 * @method FilmPersonRole[]    findAll()
 * @method FilmPersonRole[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FilmPersonRoleRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, FilmPersonRole::class);
    }

    public function findByPerson(Person $person)
    {
        return $this->createQueryBuilder('fr')
            ->join('App:Role', 'r', 'WITH', 'r.id = fr.role')
            ->join('App:Film', 'f', 'WITH', 'f.id = fr.film')
            ->where('fr.person = :person')
            ->setParameter('person', $person)
            ->orderBy('r.name', 'ASC')
            ->addOrderBy('f.name', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function findByFilm(Film $film)
    {
        return $this->createQueryBuilder('fr')
            ->join('App:Role', 'r', 'WITH', 'r.id = fr.role')
            ->join('App:Person', 'p', 'WITH', 'p.id = fr.person')
            ->where('fr.film = :film')
            ->setParameter('film', $film)
            ->orderBy('r.name', 'ASC')
            ->addOrderBy('p.fullName', 'ASC')
            ->getQuery()
            ->getResult();
    }
}
