<?php
namespace App\Twig;

use App\Entity\Person;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
    public function getFilters()
    {
        return array(
            new TwigFilter('trunc', [$this, 'truncString']),
            new TwigFilter('filmYear', [$this, 'filmYear']),
        );
    }
    public function getFunctions()
    {
        return array(
            new TwigFunction('personDates', [$this, 'personDates']),
        );
    }

    public function truncString($string, $length)
    {
        return mb_strimwidth($string, 0, $length, "...");
    }

    public function filmYear(\DateTimeInterface $filmYear)
    {
        return $filmYear->format('Y');
    }

    public function personDates(Person $person)
    {
        return $person->getBirthDate()->format('d.m.Y') . ' - ' . ($person->getDeathDate() ? $person->getBirthDate()->format('d.m.Y') : '');
    }
}