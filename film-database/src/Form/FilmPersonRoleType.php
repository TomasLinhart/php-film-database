<?php

namespace App\Form;

use App\Entity\Film;
use App\Entity\FilmPersonRole;
use App\Entity\Person;
use App\Entity\Role;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FilmPersonRoleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('role', EntityType::class, [
                'class' => Role::class,
                'placeholder' => 'Choose a role',
                'choice_label' => 'name',
            ]);

        if ($options['sourceRoute'] === 'person_add_role_film') {
            $builder
                ->add('film', EntityType::class, [
                    'class' => Film::class,
                    'placeholder' => 'Choose a film',
                    'choice_label' => function (Film $film) {
                        return $film->getLabel();
                    }
                ]);
        }
        if ($options['sourceRoute'] === 'film_add_role_person') {
            $builder
                ->add('person', EntityType::class, [
                    'class' => Person::class,
                    'placeholder' => 'Choose a person',
                    'choice_label' => function (Person $person) {
                        return $person->getLabel();
                    }
                ]);
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'sourceRoute' => '',
            'data_class' => FilmPersonRole::class,
        ]);
    }
}
