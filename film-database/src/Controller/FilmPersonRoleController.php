<?php

namespace App\Controller;

use App\Entity\FilmPersonRole;
use App\Form\FilmPersonRoleType;
use App\Repository\FilmRepository;
use App\Repository\PersonRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FilmPersonRoleController extends AbstractController
{



    /**
     * @Route("/admin/person-role-film/{personId}/new", name="person_add_role_film", methods="GET|POST")
     * @Route("/admin/film-role-person/{filmId}/new", name="film_add_role_person", methods="GET|POST")
     */
    public function new(Request $request, PersonRepository $personRepository, FilmRepository $filmRepository, int $personId = null, int $filmId = null): Response
    {
        $filmPersonRole = new FilmPersonRole();
        $label = '';
        $person = $personRepository->findOneBy(['id' => $personId]);
        if ($person) {
            $filmPersonRole->setPerson($person);
            $label = $person->getLabel() . ' - add film role';
        }
        $film = $filmRepository->findOneBy(['id' => $filmId]);
        if ($film) {
            $filmPersonRole->setFilm($film);
            $label = $person->getLabel() . ' - add person role';
        }
        $form = $this->createForm(FilmPersonRoleType::class, $filmPersonRole, [
            'sourceRoute' => $request->get('_route')
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($filmPersonRole);
            $em->flush();
            if ($personId) {
                return $this->redirectToRoute('person_show', ['id' => $personId]);
            }
            if ($filmId) {
                return $this->redirectToRoute('film_show', ['id' => $filmId]);
            }
            return $this->redirectToRoute('homepage');
        }

        return $this->render('film_person_role/new.html.twig', [
            'film_person_role' => $filmPersonRole,
            'form' => $form->createView(),
            'label' => $label
        ]);
    }

    /**
     * @Route("/admin/film-person-role/{id}", name="film_person_role_delete", methods="DELETE")
     */
    public function delete(Request $request, FilmPersonRole $filmPersonRole): Response
    {
        if ($this->isCsrfTokenValid('delete'.$filmPersonRole->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($filmPersonRole);
            $em->flush();
        }
        return $this->redirect($request->headers->get('referer'));
    }
}
