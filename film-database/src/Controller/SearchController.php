<?php

namespace App\Controller;

use App\Service\SearchService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SearchController extends AbstractController
{

    /**
     * @Route("/search", methods={"GET"}, name="film_db_search")
     */
    public function search(Request $request, SearchService $searchService): Response
    {
        if (!$request->isXmlHttpRequest()) {
            return $this->render('search/search.html.twig');
        }

        $query = $request->query->get('q', '');
        $limit = $request->query->get('l', 10);

        $results = $searchService->search($query, $limit);

        return $this->json($results);
    }
}
