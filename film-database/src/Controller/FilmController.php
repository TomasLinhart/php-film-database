<?php

namespace App\Controller;

use App\Entity\Film;
use App\Form\FilmType;
use App\Repository\FilmPersonRoleRepository;
use App\Repository\FilmRepository;
use App\Utils\Slugger;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FilmController extends AbstractController
{
    /**
     * @Route("/films", name="film_index", methods={"GET"})
     */
    public function index(FilmRepository $filmRepository): Response
    {
        $latestFilms = $filmRepository->findLatest(5);
        $futureFilms = $filmRepository->findFuture(5);
        return $this->render('film/index.html.twig', [
            'latestFilms' => $latestFilms,
            'futureFilms' => $futureFilms
        ]);
    }

    /**
     * @Route("/film/{slug}", name="film_detail", methods={"GET"})
     */
    public function detail(Film $film, FilmPersonRoleRepository $filmPersonRoleRepository): Response
    {
        $roles = $filmPersonRoleRepository->findByFilm($film);
        return $this->render('film/detail.html.twig', [
            'film' => $film,
            'roles' => $roles,
        ]);
    }

    /**
     * @Route("/admin/film/new", name="film_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $film = new Film();
        $form = $this->createForm(FilmType::class, $film);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $film->setSlug(Slugger::slugify($film->getName()));
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($film);
            $entityManager->flush();

            return $this->redirectToRoute('film_index');
        }

        return $this->render('film/new.html.twig', [
            'film' => $film,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/film/{id}", name="film_show", methods={"GET"})
     */
    public function show(Film $film, FilmPersonRoleRepository $filmPersonRoleRepository): Response
    {
        $roles = $filmPersonRoleRepository->findByFilm($film);
        return $this->render('film/show.html.twig', [
            'film' => $film,
            'roles' => $roles,
        ]);
    }

    /**
     * @Route("/admin/film/{id}/edit", name="film_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Film $film): Response
    {
        $form = $this->createForm(FilmType::class, $film);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('film_index', ['id' => $film->getId()]);
        }

        return $this->render('film/edit.html.twig', [
            'film' => $film,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/film/{id}", name="film_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Film $film): Response
    {
        if ($this->isCsrfTokenValid('delete'.$film->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($film);
            $entityManager->flush();
        }

        return $this->redirectToRoute('film_index');
    }
}
