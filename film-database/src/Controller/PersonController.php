<?php

namespace App\Controller;

use App\Entity\Person;
use App\Form\PersonType;
use App\Repository\FilmPersonRoleRepository;
use App\Repository\PersonRepository;
use App\Utils\Slugger;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PersonController extends AbstractController
{
    /**
     * @Route("/persons", name="person_index", methods={"GET"})
     */
    public function index(PersonRepository $personRepository): Response
    {
        $bornThisMonthPersons = $personRepository->findBornThisMonth();
        $persons = $personRepository->findRandom(5, $bornThisMonthPersons);
        return $this->render('person/index.html.twig', [
            'persons' => $persons,
            'bornThisMonthPersons' => $bornThisMonthPersons
        ]);
    }

    /**
     * @Route("/person/{slug}", name="person_detail", methods={"GET"})
     */
    public function detail(Person $person, FilmPersonRoleRepository $filmPersonRoleRepository): Response
    {
        $roles = $filmPersonRoleRepository->findByPerson($person);
        return $this->render('person/detail.html.twig', [
            'person' => $person,
            'roles' => $roles,
        ]);
    }

    /**
     * @Route("/admin/person/new", name="person_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $person = new Person();
        $form = $this->createForm(PersonType::class, $person);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $person->setSlug(Slugger::slugify($person->getFullName()));
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($person);
            $entityManager->flush();

            return $this->redirectToRoute('person_index');
        }

        return $this->render('person/new.html.twig', [
            'person' => $person,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/person/{id}", name="person_show", methods={"GET"})
     */
    public function show(Person $person, FilmPersonRoleRepository $filmPersonRoleRepository): Response
    {
        $roles = $filmPersonRoleRepository->findByPerson($person);
        return $this->render('person/show.html.twig', [
            'person' => $person,
            'roles' => $roles,
        ]);
    }

    /**
     * @Route("/admin/person/{id}/edit", name="person_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Person $person): Response
    {
        $form = $this->createForm(PersonType::class, $person);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('person_index', ['id' => $person->getId()]);
        }

        return $this->render('person/edit.html.twig', [
            'person' => $person,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/person/{id}", name="person_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Person $person): Response
    {
        if ($this->isCsrfTokenValid('delete'.$person->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($person);
            $entityManager->flush();
        }

        return $this->redirectToRoute('person_index');
    }
}
