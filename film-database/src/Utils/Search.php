<?php
namespace App\Utils;

class Search
{
    /**
     * Removes all non-alphanumeric characters except whitespaces.
     */
    public static function sanitizeSearchQuery(string $query): string
    {
        return trim(preg_replace('/[[:space:]]+/', ' ', $query));
    }

    /**
     *
     * Splits the search query into terms and removes the ones which are irrelevant.
     */
    public static function extractSearchTerms(string $searchQuery): array
    {
        $terms = array_unique(explode(' ', $searchQuery));

        return array_filter($terms, function ($term) {
            return 2 <= mb_strlen($term);
        });
    }
}
