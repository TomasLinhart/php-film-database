<?php
namespace App\DataFixtures;

use App\Entity\Film;
use App\Entity\FilmPersonRole;
use App\Entity\Person;
use App\Entity\Role;
use App\Entity\User;
use App\Utils\Slugger;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $this->loadUsers($manager);
        $this->loadFilms($manager);
        $this->loadPersons($manager);
        $this->loadRoles($manager);
        $this->loadFilmPersonRoleCombination($manager);
    }

    private function loadUsers(ObjectManager $manager)
    {
        foreach ($this->getUserData() as [$fullname, $email, $password, $roles]) {
            $user = new User();
            $user->setFullName($fullname);
            $user->setEmail($email);
            $user->setPassword($this->passwordEncoder->encodePassword($user, $password));
            $user->setRoles($roles);

            $manager->persist($user);
        }

        $manager->flush();
    }

    private function loadFilms(ObjectManager $manager)
    {
        foreach ($this->getFilmData() as [$name, $year, $description]) {
            $film = new Film();
            $film->setName($name);
            $film->setSlug(Slugger::slugify($name));
            $film->setYear($this->getDatetimeForYear($year));
            $film->setDescription($description);

            $manager->persist($film);
            $this->addReference('film-'.$film->getName(), $film);
        }

        $manager->flush();
    }

    private function loadPersons(ObjectManager $manager)
    {
        foreach ($this->getPersonData() as [$fullname, $nationality, $birth, $death]) {
            $person = new Person();
            $person->setFullName($fullname);
            $person->setSlug(Slugger::slugify($fullname));
            $person->setNationality($nationality);
            $person->setBirthDate($this->getDatetimeForDate($birth));
            $person->setDeathDate($this->getDatetimeForDate($death));

            $manager->persist($person);
            $this->addReference('person-'.$person->getFullName(), $person);
        }

        $manager->flush();
    }

    private function loadRoles(ObjectManager $manager)
    {
        foreach ($this->getRoleData() as [$name]) {
            $role = new Role();
            $role->setName($name);

            $manager->persist($role);
            $this->addReference('role-'.$role->getName(), $role);
        }

        $manager->flush();
    }

    private function loadFilmPersonRoleCombination(ObjectManager $manager)
    {
        foreach ($this->getFilmPersonRoleData() as [$filmName, $personFullName, $roleName]) {
            $filmPersonRole = new FilmPersonRole();
            $filmPersonRole->setFilm($this->getReference('film-'.$filmName));
            $filmPersonRole->setPerson($this->getReference('person-'.$personFullName));
            $filmPersonRole->setRole($this->getReference('role-'.$roleName));

            $manager->persist($filmPersonRole);
        }
        $manager->flush();
    }

    private function getUserData(): array
    {
        return [
            // $userData = [$fullname, $email, $password, $roles];
            ['Tomáš Linhart', 'linhart@admin.cz', '1234', ['ROLE_ADMIN']],
        ];
    }

    private function getFilmData(): array
    {
        return [
            // $filmData = [$name, $year, $description];
            ['Avengers: Age of Ultron', '2015', 'Pokus miliardáře Tonyho Starka oživit program na udržení celosvětového míru skončí nezdarem a největší hrdinové světa, včetně Iron Mana, Kapitána Ameriky, Thora, Neuvěřitelného Hulka, Black Widow a Hawkeye musí čelit děsivému nebezpečí. V sázce je osud celé planety. Povstane zlovolný Ultron a jenom Avengers mu mohou zabránit v uskutečnění ďábelských plánů. Nová křehká spojenectví a nečekané zvraty jsou zárukou vzrušujícího a jedinečného akčního dobrodružství globálních rozměrů.(Falcon)'],
            ['Captain Marvel', '2019', 'Snímek Captain Marvel se odehrává v devadesátých letech dvacátého století a je tak zcela novým dobrodružstvím z dosud neviděného období filmového světa Marvel. Film sleduje příběh Carol Denversové, ze které stala jedna z nejmocnějších superhrdinek světa. Když Zemi dostihne válka mezi dvěma mimozemskými rasami, Denversová se ocitá s malou skupinou spojenců přímo v srdci konfliktu.(Falcon)'],
            ['Avengers: Infinity War', '2018', 'Snímek Avengers: Infinity War završuje neuvěřitelnou desetiletou cestu filmovým světem studia Marvel a přináší na stříbrná plátna nejsmrtonosnější a nejultimátnější válku všech dob. Avengers a jejich superhrdinští spojenci musí riskovat úplně vše a pokusit se porazit mocného Thanose dřív, než jeho bleskový útok, provázený ničením a zkázou, zničí vesmír jednou provždy.(Falcon)'],
            ['Captain America: Občanská válka', '2016', 'Film studia Marvel Captain America: Občanská válka se odehrává v době, kdy Steve Rogers převzal vedení nově zformovaného týmu Avengers, který se nadále snaží chránit lidstvo. Dojde však k dalšímu incidentu, ve kterém sehrají Avengers nešťastnou roli. Politické tlaky vyústí ve vytvoření systému zodpovědnosti vedenému vládní organizací, jež na tým super-hrdinů dohlíží a řídí ho. Nová skutečnost Avengers rozdělí na dva tábory. Jeden vede Steve Rogers, jež hájí svobodu Avengers, aby mohli bránit lidstvo bez vládních zásahů. Druhý vede Tony Stark, který překvapivě zastává myšlenku vládního dohledu a zodpovědnosti. Připravte se vybrat si svou stranu a přidat se k non-stop akci na dvou frontách.(Falcon)'],
            ['Ant-Man', '2015', 'Podvodník Scott Lang (Paul Rudd), vybavený pozoruhodnou schopnosti zmenšit své fyzické rozměry, ale současně znásobit svou sílu, se musí ujmout role hrdiny a pomoci svému mentorovi doktorovi Hankovi Pymovi (Michael Douglas) ochránit tajemství, skrývající se v jeho úžasném obleku Ant-Mana, před zcela novými nebezpečími, která ho ohrožují. Pym a Lang jsou tváří v tvář zdánlivě nepřekonatelným překážkám nuceni naplánovat a provést rafinovanou loupež, která zachrání celý svět.(Falcon)'],
            ['Doctor Strange', '2016', 'Studio Marvel uvádí film Doctor Strange o světoznámém neurochirurgovi Dr. Stephenu Strangovi, jehož život se změní po strašlivé dopravní nehodě, která mu znemožnila používat ruce. Když ho tradiční medicína zklame, je Strange nucen hledat uzdravení a naději jinde. A tak se vypraví na mystické, odloučené místo známé jako Kamar-Taj. Brzy zjistí, že Kamar-Taj není jen chrámem uzdravení, ale i základnou pro boj s temnými silami, jež chtějí zničit naši realitu. Zanedlouho si musí Strange – vybavený novými magickými schopnostmi – vybrat, zda se vrátí k původnímu životu plnému bohatství a slávy, anebo se všeho vzdá a bude bránit svět jako nejmocnější kouzelník ve vesmíru.(Falcon)'],
        ];
    }

    private function getRoleData(): array
    {
        return [
            // $roleData = [$name];
            ['Režie'],
            ['Kamera'],
            ['Hrají'],
        ];
    }

    private function getPersonData(): array
    {
        return [
            // $personData = [$fullname, $nationality, $birth, $death];
            ['Joss Whedon', 'USA', '23.6.1964', null],
            ['Ryan Fleck', 'USA', '20.9.1976', null],
            ['Anthony Russo', 'USA', '3.2.1970', null],
            ['Joe Russo', 'USA', '8.7.1971', null],
            ['Peyton Reed', 'USA', '3.7.1964', null],
            ['Scott Derrickson', 'USA', '16.7.1966', null],

            ['Chris Evans', 'USA', '13.6.1981', null],
            ['Brie Larson', 'USA', '1.10.1989', null],
            ['Robert Downey Jr.', 'USA', '4.4.1965', null],
            ['Paul Rudd', 'USA', '6.4.1969', null],
            ['Benedict Cumberbatch', 'Velká Británie', '19.7.1976', null],
        ];
    }

    private function getFilmPersonRoleData(): array
    {
        return [
            // $filmPersonRoleData = [$filmName, $personFullName, $roleName];
            ['Avengers: Age of Ultron', 'Joss Whedon', 'Režie'],
            ['Captain Marvel', 'Ryan Fleck', 'Režie'],
            ['Avengers: Infinity War', 'Anthony Russo', 'Režie'],
            ['Captain America: Občanská válka', 'Anthony Russo', 'Režie'],
            ['Avengers: Infinity War', 'Joe Russo', 'Režie'],
            ['Captain America: Občanská válka', 'Joe Russo', 'Režie'],
            ['Ant-Man', 'Peyton Reed', 'Režie'],
            ['Doctor Strange', 'Scott Derrickson', 'Režie'],

            ['Avengers: Age of Ultron', 'Chris Evans', 'Hrají'],
            ['Avengers: Infinity War', 'Chris Evans', 'Hrají'],
            ['Captain America: Občanská válka', 'Chris Evans', 'Hrají'],
            ['Captain Marvel', 'Brie Larson', 'Hrají'],
            ['Avengers: Age of Ultron', 'Robert Downey Jr.', 'Hrají'],
            ['Avengers: Infinity War', 'Robert Downey Jr.', 'Hrají'],
            ['Captain America: Občanská válka', 'Robert Downey Jr.', 'Hrají'],
            ['Captain America: Občanská válka', 'Paul Rudd', 'Hrají'],
            ['Ant-Man', 'Paul Rudd', 'Hrají'],
            ['Avengers: Infinity War', 'Benedict Cumberbatch', 'Hrají'],
            ['Doctor Strange', 'Benedict Cumberbatch', 'Hrají'],
        ];
    }

    private function getDatetimeForYear(string $year): Datetime
    {
        return DateTime::createFromFormat('Y', $year);
    }

    private function getDatetimeForDate(?string $date): ?Datetime
    {
        if ($date === null) {
            return $date;
        }
        return DateTime::createFromFormat('d.m.Y', $date);
    }
}
