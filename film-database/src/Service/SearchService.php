<?php
namespace App\Service;

use App\Entity\Film;
use App\Entity\Person;
use App\Repository\FilmRepository;
use App\Repository\PersonRepository;
use App\Utils\Search;
use Symfony\Component\Routing\RouterInterface;

class SearchService
{
    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var FilmRepository
     */
    private $filmRepository;

    /**
     * @var PersonRepository
     */
    private $personRepository;

    /**
     * Search constructor.
     */
    public function __construct(
        RouterInterface $router,
        FilmRepository $filmRepository,
        PersonRepository $personRepository
    ) {
        $this->filmRepository = $filmRepository;
        $this->personRepository = $personRepository;
        $this->router = $router;
    }

    public function search($query, $limit) {
        $searchTerms = Search::extractSearchTerms(Search::sanitizeSearchQuery($query));
        if (0 === count($searchTerms)) {
            return [];
        }

        /** @var Film[] $foundFilms */
        $foundFilms = $this->filmRepository->findBySearchTerms($searchTerms, $limit);
        /** @var Person[] $foundPersons */
        $foundPersons = $this->personRepository->findBySearchTerms($searchTerms, $limit);
        $results = [];
        foreach ($foundFilms as $film) {
            $results[] = [
                'type' => 'film',
                'title' => htmlspecialchars($film->getName(), ENT_COMPAT | ENT_HTML5),
                'description' => htmlspecialchars(substr($film->getDescription(), 0, 100), ENT_COMPAT | ENT_HTML5),
                'year' => $film->getYear()->format('Y'),
                'slug' => $film->getSlug(),
                'url' => $this->router->generate('film_detail', ['slug' => $film->getSlug()]),
                'urlShow' => $this->router->generate('film_show', ['id' => $film->getId()]),
            ];
        }
        foreach ($foundPersons as $person) {
            $results[] = [
                'type' => 'person',
                'fullName' => htmlspecialchars($person->getFullName(), ENT_COMPAT | ENT_HTML5),
                'nationality' => htmlspecialchars($person->getNationality(), ENT_COMPAT | ENT_HTML5),
                'birthDate' => $person->getBirthDate()->format('d.m.Y'),
                'slug' => $person->getSlug(),
                'url' => $this->router->generate('person_detail', ['slug' => $person->getSlug()]),
                'urlShow' => $this->router->generate('person_show', ['id' => $person->getId()]),
            ];
        }
        return $results;
    }

}